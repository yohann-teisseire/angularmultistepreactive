import { Component, Input, OnInit } from '@angular/core';
import { FormDataService } from './services/form-data.service';
import { Work } from './entities/work';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';

  @Input() formData;

  constructor(private _formDataService: FormDataService) {}

  ngOnInit(): void {
    
    this.formData = this._formDataService.getFormData();
    const work: Work = {
      name: 'dev',
      type: 'fullstack'
    };
    this._formDataService.setWork(work);
    
  }
}
