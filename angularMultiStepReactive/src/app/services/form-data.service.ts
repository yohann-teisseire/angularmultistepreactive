import { Injectable } from '@angular/core';
import { FormData } from '../entities/form-data';
import { Personal } from '../entities/personal';
import { Work } from '../entities/work';
import { Address } from '../entities/address';

@Injectable()
export class FormDataService {

  private formData: FormData = new FormData();
  private isPersonalFormValid = false;
  private isWorkFormValid = false;
  private isAddressFormValid = false;


  getPersonal(): Personal {
    const personal: Personal = {
      firstName: this.formData.personal.firstName,
      lastName: this.formData.personal.lastName,
      email: this.formData.personal.email
    };

    return personal;
  }

  setPersonal(data: Personal): void {
    this.isPersonalFormValid = true;

    this.formData.personal.firstName = data.firstName;
    this.formData.personal.lastName = data.lastName;
    this.formData.personal.email = data.email;
  }

  getWork(): Work {
    const work: Work = {
      name: this.formData.work.name,
      type: this.formData.work.type
    };

    return work;
  }

  setWork(data: Work): void {
    this.isWorkFormValid = true;

    this.formData.work.name = data.name;
    this.formData.work.type = data.type;
  }

  getAddress(): Address {
    const address: Address = {
        street: this.formData.address.street,
        city: this.formData.address.city,
        state: this.formData.address.state,
        zip: this.formData.address.zip
    };

    return address;
  }

  setAddress(data: Address) {
    this.isAddressFormValid = true;

    this.formData.address.street = data.street;
    this.formData.address.city = data.city;
    this.formData.address.state = data.state;
    this.formData.address.zip = data.zip;
  }

  getFormData(): FormData {
    return this.formData;
  }

  resetFormData(): FormData {

    this.formData.clear();

    this.isPersonalFormValid = false;
    this.isWorkFormValid = false;
    this.isAddressFormValid = false;

    return this.formData;
  }

  isFormValid() {
    return this.isPersonalFormValid && this.isWorkFormValid && this.isAddressFormValid;
  }
}
