import { Component, OnInit } from '@angular/core';
import { Personal } from '../entities/personal';
import { Router } from '@angular/router';
import { FormDataService } from '../services/form-data.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalComponent implements OnInit {

  title = 'Please tell us about yourself.';
  personal: Personal;
  personalForm: FormGroup;
  constructor(
    private _router: Router,
    private _formDataService: FormDataService,
    private _fb: FormBuilder
  ) { }

  ngOnInit() {
    this.createForm();
    this.personal = this._formDataService.getPersonal();
  }

  createForm(): FormGroup {
    this.personalForm = this._fb.group({
      firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        email: ['', Validators.pattern('^[^\s@]+@[^\s@]+\.[^\s@]{2,}$')]
    });

    return this.personalForm;
  }

  get firstName() { return this.personalForm.get('firstName'); }
  get lastName() { return this.personalForm.get('lastName'); }
  get email() { return this.personalForm.get('email'); }

  save(form: any): boolean {
    if (!form.valid) {
      return false;
    }
    console.log('save');
    this._formDataService.setPersonal(<Personal>form.value);
    return true;
  }

  goNext(form: any) {
    if (this.save(form)) {
      this._router.navigate(['work']);
    }
  }

}
