export class Address {
    street: string;
    city: string;
    state: string;
    zip: number;

    constructor(
        street = '',
        city = '',
        state = '',
        zip = ''
    ) {}
}
