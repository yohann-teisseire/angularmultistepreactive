import { Personal } from './personal';
import { Work } from './work';
import { Address } from './address';

export class FormData {

    personal: Personal;
    work: Work;
    address: Address;

    constructor() {
        this.personal = new Personal();
        this.work = new Work();
        this.address = new Address();
    }

    clear(): void {
        this.personal = new Personal();
        this.work = new Work();
        this.address = new Address();
    }
}
